import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FabricantFigures implements MouseListener {
  // Attributs
	private FigureColoree figure_en_cours_de_fabrication;
	private int nb_points_cliques;
	private Point[] points_cliques;
	
  // Constructeurs
	/**
	 * Constructeur simple
	 * @param fc -> figure a fabriquer
	 */
	public FabricantFigures(FigureColoree fc)
	{
		if (fc!=null)
		{
			this.figure_en_cours_de_fabrication = fc;
			this.nb_points_cliques = 0;
			this.points_cliques = new Point[fc.nbClics()];
		}
	}
	
  // Methodes heritees
	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}
	
	@Override
	public void mousePressed(MouseEvent e)
	{
		if (this.figure_en_cours_de_fabrication != null)
		{
			if (this.nb_points_cliques < this.points_cliques.length-1)
			{
				this.points_cliques[this.nb_points_cliques] = new Point(e.getX(), e.getY());
				this.nb_points_cliques++;
			}
			else
			{
				this.points_cliques[this.nb_points_cliques] = new Point(e.getX(), e.getY());
				this.figure_en_cours_de_fabrication.modifierPoints(this.points_cliques);
				((DessinFigures)(e.getSource())).ajoute(this.figure_en_cours_de_fabrication);
				((JComponent)(e.getSource())).removeMouseListener(this);
			}
		}
	}

}
