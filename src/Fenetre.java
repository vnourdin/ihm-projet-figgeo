import java.awt.*;

import javax.swing.*;

/**
 * Classe principale qui sert de JFrame et lance le programme
 */
public class Fenetre extends JFrame{
  // Constructeur
	/**
	 * Constructeur par defaut
	 * @param nom -> nom de la fenetre
	 * @param largeur -> largeur de la fenetre
	 * @param hauteur -> hauteur de la fenetre
	 */
	public Fenetre(String nom, int largeur, int hauteur)
	{
		super(nom);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(largeur,hauteur));
	}
	
  // Methode
	public static void main(String [] args)
	{
		// On cree le DessinFigures sur lequel on va dessiner et le PanneauChoix
		DessinFigures dessin;
		PanneauChoix panneau;
		Fenetre f = new Fenetre("Projet Figures Géometriques", 1000, 700);
		
		// On initialise le dessin, le panneau de menus et on les associe
		dessin = new DessinFigures();
		panneau = new PanneauChoix(dessin);
		dessin.setPanneauChoix(panneau);
		
		// On cree un JPanel "supperieur" qui englobe les deux precedants
		JPanel panneauTotal = new JPanel();
		
		// On ajoute le dessin et les menus au panneauTotal
		panneauTotal.setLayout(new BorderLayout());
		panneauTotal.add(panneau, BorderLayout.NORTH);
		panneauTotal.add(dessin, BorderLayout.CENTER);
		
		f.setContentPane(panneauTotal);
		f.pack();
		f.setVisible(true);
	}
}
