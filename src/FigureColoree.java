import java.awt.*;

public abstract class FigureColoree {
  // Attributs
	// Taille des carrés de selection
	protected final static int TAILLE_CARRE_SELECTION = 4;
	// Périphérie des carrés de selection
	protected final static int PERIPHERIE_CARRE_SELECTION = 8;
	// Tableau memorisant les points de la figure
	protected Point [] tab_mem;
	// Couleur de la figure
	protected Color couleur;
	// Booleen qui indique si la fig est selectionnee
	private boolean selected;
	// Booleen qui indique si la fig est remplie ou non
	protected boolean remplissage;
	
  // Constructeurs
	/**
	 * Constructeur par defaut
	 */
	public FigureColoree()
	{
		this.tab_mem = new Point[this.nbPoints()];
		this.selected = false;
		this.couleur = Color.BLACK;
		this.remplissage = true;
	}
	
	/**
	 * Constructeur permettant de construire une figure qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur de la figure
	 * @param r -> remplisage de la figure
	 */
	public FigureColoree(Color c, boolean r)
	{
		this.tab_mem = new Point[this.nbPoints()];
		this.selected = false;
		this.couleur = c;
		this.remplissage = r;
	}
	
  // Methodes
	/**
	 * Methode qui retourne le nombre de points qui delimitent la figure
	 * @return int -> le nombre de pts de la figure
	 */
	public abstract int nbPoints();
	
	/**
	 * Methode qui retourne le nombre de clics necessaires pour creer la figure
	 * @return int -> le nombre de clics de la figure
	 */
	public abstract int nbClics();
	
	/**
	 * Methode d'affichage de la figure. Lorsqu'elle est selectionnee, de petits carrés apparaissent aux angles de la figure.
	 * @param g -> environement graphique
	 */
	public void affiche(Graphics g)
	{
		if(selected)
		{
			g.setColor(Color.BLACK);
			for(int i=0; i<tab_mem.length; i++)
			{
				g.fillRect(
						(int)(tab_mem[i].getX())-(TAILLE_CARRE_SELECTION/2),
						(int)(tab_mem[i].getY())-(TAILLE_CARRE_SELECTION/2),
						TAILLE_CARRE_SELECTION,
						TAILLE_CARRE_SELECTION );
			}
		}
	}
	
	/**
	 * Methode qui permet de changer la couleur de la figure
	 * @param c -> nouvelle couleur de la figure
	 */
	public void changeCouleur(Color c)
	{
		this.couleur = c;
	}
	
	/**
	 * Methode qui permet de deselectionner la figure
	 */
	public void deSelectionne()
	{
		this.selected = false;
	}
	
	/**
	 * Methode qui permet de selectionner la figure
	 */
	public void selectionne()
	{
		this.selected = true;
	}
	
	/**
	 * Methode qui indique si la figure est selectionnee
	 * @return booleen -> true si la figure est selectionnee
	 */
	public boolean estSelectionne()
	{
		return this.selected;
	}
	
	/**
	 * Methode qui permet de modifier le remplissage de la figure (pleine ou vide)
	 * @param r -> true si la figure doit etre pleine
	 */
	public void setRemplissage(boolean r)
	{
		this.remplissage= r;
	}
	
	/**
	 * Methode qui permet de connaitre le remplissage de la figure
	 * @return boolean -> remplissage de la figure
	 */
	public boolean getRemplissage()
	{
		return this.remplissage;
	}

	/**
	 * Methode qui indique si un point entre en parametre se trouve dans la figure ou non
	 * @param x -> int, abscisse du point que l'on veut tester
	 * @param y -> ordonnee du point que l'on veut tester 
	 * @return boolean -> true si le point est dans la figure
	 */
	public abstract boolean estDedans(int x, int y);
	
	/**
	 * Methode qui permet de modifier les points de memorisation (tab_mem) sur la figure a partir de points de de saisie
	 * @param ps -> Point[], tableau contenant les nouveaux points
	 */
	public abstract void modifierPoints(Point[] ps);
	
	/**
	 * Methodes qui permet de deplacer la figure
	 * @param dx -> deplacement sur l'axe des abscisses
	 * @param dy -> deplacement sur l'axe des ordonnees
	 */
	public void translation(int dx, int dy)
	{
		for (int i=0; i<this.nbPoints(); i++)
		{
			this.tab_mem[i].translate(dx, dy);
		}
	}
}