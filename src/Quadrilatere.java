import java.awt.Color;


public class Quadrilatere extends Polygone {

  // Constructeur
	/**
	 * Constructeur permettant de construire un quadrilatere qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du quadrilatere
	 * @param r -> remplisage du quadrilatere
	 */
	public Quadrilatere(Color c, boolean r)
	{
		super(c,r);
	}
	
  // Methodes
	@Override
	public int nbPoints() {
		return 4;
	}

	@Override
	public int nbClics() {
		return 4;
	}
}
