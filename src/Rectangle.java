import java.awt.Color;
import java.awt.Point;


public class Rectangle extends Quadrilatere{

  // Constructeur
	/**
	 * Constructeur permettant de construire un rectangle qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du rectangle
	 * @param r -> remplisage du rectangle
	 */
	public Rectangle(Color c, boolean r)
	{
		super(c,r);
	}

  // Methodes
	@Override
	public int nbClics()
	{
		return 2;
	}

	@Override
	public void modifierPoints(Point[] ps)
	{
		if(ps!=null && ps.length == this.nbClics())
		{
			this.tab_mem[0] = ps[0];
			this.tab_mem[1] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
			this.tab_mem[2] = ps[1];
			this.tab_mem[3] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
		}
	}
}
