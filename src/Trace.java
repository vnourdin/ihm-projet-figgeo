import java.awt.*;

public class Trace {
  // Attributs
	/**
	 * Points de debut et de fin du trace
	 */
	private Point deb, fin;
	/**
	 * Couleur du trace
	 */
	private Color coul;
	
  // Constructeur
	/**
	 * Constructeur par defaut
	 * @param x1 -> ordonnee du point de debut
	 * @param y1 -> abscisse du point de debut 
	 * @param x2 -> ordonnee du point de fin
	 * @param y2 -> abscisse du point de fin
	 * @param c -> couleur du trace
	 */
	public Trace(int x1, int y1, int x2, int y2, Color c)
	{
		this.deb = new Point(x1,y1);
		this.fin = new Point(x2,y2);
		this.coul = c;
	}

  // Methodes
	/**
	 * Methode qui permet de recuperer le point de debut du trace
	 * @return Point -> debut du trace
	 */
	public Point getDeb() {
		return deb;
	}

	/**
	 * Methode qui permet de recuperer le point de fin du trace
	 * @return Point -> fin du trace
	 */
	public Point getFin() {
		return fin;
	}
	
	/**
	 * Methode qui permet de recuperer la couleur du trace
	 * @return Color -> couleur du trace
	 */
	public Color getCoul(){
		return this.coul;
	}
}
