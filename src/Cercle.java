import java.awt.Color;
import java.awt.Point;


public class Cercle extends Elipse{
  // Constructeur
	/**
	 * Constructeur permettant de construire un cercle qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du cercle
	 * @param r -> remplisage du cercle
	 */
	public Cercle(Color c, boolean r) {
		super(c, r);
	}

  // Methodes
	@Override
	public void modifierPoints(Point[] ps)
	{
		if(ps!=null && ps.length == this.nbClics())
		{
			// On calcule le rayon du cercle /!\ le premier clic est le centre du cercle /!\ donc le deuxieme definit forcement le rayon
			int rayon = (int) ps[0].distance(ps[1]);
			
			// Ensuite on cree le carre qui contient le cercle, a partir de son centre
			this.tab_mem[0] = new Point((int)ps[0].getX()-rayon, (int)ps[0].getY()-rayon);
			this.tab_mem[1] = new Point((int)ps[0].getX()+rayon, (int)ps[0].getY()-rayon);
			this.tab_mem[2] = new Point((int)ps[0].getX()+rayon, (int)ps[0].getY()+rayon);
			this.tab_mem[3] = new Point((int)ps[0].getX()-rayon, (int)ps[0].getY()+rayon);
		}
	}

}
