import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Elipse extends FigureColoree{
  // Constructeur

	/**
	 * Constructeur permettant de construire une elipse qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur de l'elipse
	 * @param r -> remplisage de l'elipse
	 */
	public Elipse(Color c, boolean r)
	{
		super(c,r);
	}

  // Methodes heritees
	@Override
	public int nbPoints() {
		return 4;
	}

	@Override
	public int nbClics() {
		return 2;
	}

	@Override
	public boolean estDedans(int x, int y) {
		if(x>this.tab_mem[0].x && x<this.tab_mem[2].x && y>this.tab_mem[0].y && y<this.tab_mem[2].y)
			return true;
		else
			return false;
	}

	@Override
	public void modifierPoints(Point[] ps)
	{
		if(ps!=null && ps.length == this.nbClics())
		{
			// Plusieurs possibilites :
			// - si le premier clic est en haut a gauche du deuxieme
			if(ps[0].x < ps[1].x && ps[0].y < ps[1].y)
			{
				this.tab_mem[0] = ps[0];
				this.tab_mem[1] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
				this.tab_mem[2] = ps[1];
				this.tab_mem[3] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
			}

			// - si le premier clic est en bas a gauche du deuxieme
			else if(ps[0].x < ps[1].x && ps[0].y > ps[1].y)
			{
				this.tab_mem[0] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
				this.tab_mem[1] = ps[1];
				this.tab_mem[2] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
				this.tab_mem[3] = ps[0];
			}

			// - si le premier clic est en haut a droite du deuxieme
			else if(ps[0].x > ps[1].x && ps[0].y < ps[1].y)
			{
				this.tab_mem[0] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
				this.tab_mem[1] = ps[0];
				this.tab_mem[2] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
				this.tab_mem[3] = ps[1];
			}

			// - si le premier clic est en bas a droite du deuxieme
			else if(ps[0].x > ps[1].x && ps[0].y > ps[1].y)
			{
				this.tab_mem[0] = ps[1];
				this.tab_mem[1] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
				this.tab_mem[2] = ps[0];
				this.tab_mem[3] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
			}

			// - sinon (exemple : double clic) on fait comme dans le premier cas pour eviter une erreur
			else
			{
				this.tab_mem[0] = ps[0];
				this.tab_mem[1] = new Point( (int)(ps[0].getX() ) ,(int)(ps[1].getY()) );
				this.tab_mem[2] = ps[1];
				this.tab_mem[3] = new Point( (int)(ps[1].getX() ) ,(int)(ps[0].getY()) );
			}
		}
	}

	@Override
	public void affiche(Graphics g)
	{
		g.setColor(this.couleur);

		if (this.getRemplissage())
		{
			g.fillOval(this.tab_mem[0].x, this.tab_mem[0].y, (this.tab_mem[2].x - this.tab_mem[0].x), (this.tab_mem[2].y - this.tab_mem[0].y));
		}

		else
		{
			g.drawOval(this.tab_mem[0].x, this.tab_mem[0].y, (this.tab_mem[2].x - this.tab_mem[0].x), (this.tab_mem[2].y - this.tab_mem[0].y));
		}
		super.affiche(g);
	}



}
