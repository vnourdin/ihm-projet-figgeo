import java.awt.*;


public abstract class Polygone extends FigureColoree{
  // Attributs
	// Polygone de la class Java qu'on utilise pour les methodes drawPolygon et fillPolygon
	private Polygon p;
	
  // Constructeur
	/**
	 * Constructeur permettant de construire un polygone qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du polygone
	 * @param r -> remplisage du polygone
	 */
	public Polygone(Color c, boolean r)
	{
		super(c,r);
		this.p = new Polygon();
	}
	
  // Methodes
	@Override
	public void modifierPoints(Point[] ps)
	{
		if(ps!=null)
		{
			if(ps.length == this.nbPoints())
				this.tab_mem = ps;
		}
	}
	
	@Override
	public void affiche(Graphics g)
	{
		g.setColor(this.couleur);
		this.p = new Polygon();
		
		for(int i=0; i<this.tab_mem.length; i++)
		{
			this.p.addPoint((int)(this.tab_mem[i].getX()), (int)(this.tab_mem[i].getY()));
		}
		
		if (this.getRemplissage())
		{
			g.fillPolygon(this.p);
		}
		
		else
		{
			g.drawPolygon(this.p);
		}
		super.affiche(g);
	}
	
	@Override
	public boolean estDedans(int x, int y) {
		return this.p.contains(x, y);
	}
}
