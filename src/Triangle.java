import java.awt.Color;


public class Triangle extends Polygone{

  // Constructeur
	/**
	 * Constructeur permettant de construire un triangle qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du triangle
	 * @param r -> remplisage du triangle
	 */
	public Triangle(Color c, boolean r)
	{
		super(c,r);
	}

	@Override
	public int nbPoints() {
		return 3;
	}

	@Override
	public int nbClics() {
		return 3;
	}
}