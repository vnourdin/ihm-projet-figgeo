import java.awt.Color;
import java.awt.Point;


public class Carre extends Rectangle {
	
  // Constructeur
	/**
	 * Constructeur permettant de construire un carre qui utilise les derniers parametres entres dans les menus
	 * @param c -> couleur du carre
	 * @param r -> remplisage du carre
	 */
	public Carre(Color c, boolean r)
	{
		super(c,r);
	}

  // Methodes
	@Override
	public void modifierPoints(Point[] ps) {
		if(ps!=null && ps.length == this.nbClics())
		{
			if(ps[0].y > ps[1].y)
			{
				Point temp = ps[0];
				ps[0] = ps[1];
				ps[1] = temp;
			}
			
			ps[1] = new Point(ps[1].x, ps[0].y);
			int cote = (int)ps[0].distance(ps[1]);
			
				this.tab_mem[0] = ps[0];
				this.tab_mem[1] = ps[1];
				this.tab_mem[2] = new Point(ps[1].x, ps[1].y+cote);
				this.tab_mem[3] = new Point(ps[0].x, ps[0].y+cote);
		}
	}
}
