
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class PanneauChoix extends JPanel implements ActionListener {
  // Attributs
	// DessinFigures et FigureColoree associes
	private DessinFigures dfActuel;
	private FigureColoree figActuelle;
	// Couleur actuelle qui sert pour les nouvelles figures et pour modifier les autres
	private Color laCouleur;

	// Menus deroulants
	private JComboBox choixFig;
	private JComboBox choixRemplissage;

	// Boutons
	private JButton eff;
	private JButton effTout;
	private JButton choixCouleur;
	private JButton exporter;

	// Boutons radios et leur groupe
	private JRadioButton creerFigure;
	private JRadioButton manipuler;
	private JRadioButton dessiner;
	private ButtonGroup bg;

  // Constructeurs
	/**
	 * Constructeur par defaut
	 * @param df -> DessinFigures associé
	 */
	public PanneauChoix(DessinFigures df)
	{
		this.dfActuel = df;
		this.figActuelle = new Quadrilatere(Color.BLACK, true);
		this.laCouleur = Color.BLACK;

		// Creation des boutons radios et du groupe de boutons
		this.creerFigure = new JRadioButton("Créer une figure");
		this.manipuler = new JRadioButton("Manipuler");
		this.dessiner = new JRadioButton("Dessinner");
		this.bg = new ButtonGroup();
		this.creerFigure.addActionListener(this);
		this.manipuler.addActionListener(this);
		this.dessiner.addActionListener(this);

		// Creation des deux menus deroulants
		this.choixFig = new JComboBox(new String[] {"Quadrilatere","Rectangle","Carre","Triangle","Elipse","Cercle"});
		this.choixRemplissage = new JComboBox(new String[] {"Figure remplie","Figure vide"});
		this.choixFig.addActionListener(this);
		this.choixRemplissage.addActionListener(this);

		// Creation des boutons
		this.eff = new JButton("Effacer");
		this.effTout = new JButton("Effacer tout");
		this.choixCouleur= new JButton("Couleurs");
		this.eff.addActionListener(this);
		this.effTout.addActionListener(this);
		this.choixCouleur.addActionListener(this);
		
		this.exporter = new JButton("Exporter");
		this.exporter.addActionListener(this);

		// Desactivation du menu choixFig par defaut, car il n'est utile qu'en mode creation de figure
		this.choixFig.setEnabled(false);
		// Desactivation des boutons effacer par defaut car  ils ne sont utiles qu'en mode modification
		this.eff.setEnabled(false);
		this.effTout.setEnabled(false);

		// Ajouts des boutons radio au groupe de boutons radio
		this.bg.add(creerFigure);
		this.bg.add(manipuler);
		this.bg.add(dessiner);

		// Creation d'un JPanel qui represente la partie haute du panneau de menus
		JPanel haut = new JPanel();
		// Ajouts des boutons radio dans le JPanel du haut
		haut.add(this.creerFigure,BorderLayout.NORTH);
		haut.add(this.manipuler,BorderLayout.NORTH);
		haut.add(this.dessiner,BorderLayout.NORTH);
		
		// Creation d'un JPanel qui represente la partie basse du panneau de menus
		JPanel bas = new JPanel();
		// Ajouts des boutons et menus deroulants dans le JPanel du bas
		bas.add(this.choixFig);
		bas.add(this.choixRemplissage);
		bas.add(this.choixCouleur);
		
		// Ajout d'une separation entre les Menus et les boutons effacer
		JSplitPane jp = new JSplitPane();
		jp.setLeftComponent(this.choixCouleur);
		jp.setRightComponent(this.eff);
		bas.add(jp);
		
		bas.add(this.eff);
		bas.add(this.effTout);
		bas.add(this.exporter);
		
		// Ajout des sous-panneaux dans le panneau de menu
		this.setLayout(new BorderLayout());
		this.add(haut, BorderLayout.NORTH);
		this.add(bas, BorderLayout.SOUTH);
	}

  // Methodes

	/**
	 * Methode qui gere les actions des boutons et des menus
	 */
	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==this.creerFigure)
		{
			this.dfActuel.supprimeAuditeurs();
			this.choixFig.setEnabled(true);
			this.choixRemplissage.setEnabled(true);
			this.eff.setEnabled(false);
			this.effTout.setEnabled(false);
		}
		
		if(e.getSource()==this.manipuler)
		{
			this.choixFig.setEnabled(false);
			this.choixRemplissage.setEnabled(true);
			this.eff.setEnabled(true);
			this.effTout.setEnabled(true);
			this.dfActuel.supprimeAuditeurs();
			this.dfActuel.activeManipulationsSouris();
		}
		
		if(e.getSource()==this.dessiner)
		{
			if(this.figActuelle.estSelectionne())
				this.figActuelle.deSelectionne();
			
			this.dfActuel.supprimeAuditeurs();
			this.choixFig.setEnabled(false);
			this.choixRemplissage.setEnabled(false);
			this.eff.setEnabled(false);
			this.effTout.setEnabled(false);
			
			this.dfActuel.creerScribble();
			this.dfActuel.setScribbleColor(laCouleur);
			
			this.dfActuel.repaint();
		}
		
		if(e.getSource()==this.eff)
		{
			if(this.figActuelle.estSelectionne())
				this.dfActuel.supprimeFigSel();
			this.dfActuel.repaint();
		}
		
		if(e.getSource()==this.effTout)
		{
			dfActuel.vider();
			dfActuel.repaint();
		}
		
		if(e.getSource()==this.choixCouleur)
		{
			laCouleur =  JColorChooser.showDialog(null, "Choix de la couleur", Color.BLACK);
			if(figActuelle.estSelectionne())
				figActuelle.changeCouleur(laCouleur);
			dfActuel.setScribbleColor(laCouleur);
			dfActuel.repaint();
		}

		if(e.getSource()==this.choixRemplissage)
		{
			// Si la figure est selectionnee (ou en construction) son remplissage deviens celui actuelement selectionne dans le menu deroulant
			if(figActuelle!=null && figActuelle.estSelectionne())
				figActuelle.setRemplissage(determinerRemplissage((String)choixRemplissage.getSelectedItem()));
			dfActuel.repaint();
		}

		if(e.getSource()==this.choixFig)
		{
			// Gere les constructions de figures en fonctione des menus
			switch (choixFig.getSelectedIndex()){
			case 0:
				figActuelle.deSelectionne();
				figActuelle = new Quadrilatere(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			case 1 :
				figActuelle.deSelectionne();
				figActuelle = new Rectangle(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			case 2:
				figActuelle.deSelectionne();
				figActuelle = new Carre(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			case 3:
				figActuelle.deSelectionne();
				figActuelle = new Triangle(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			case 4:
				figActuelle.deSelectionne();
				figActuelle = new Elipse(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			case 5:
				figActuelle.deSelectionne();
				figActuelle = new Cercle(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
				break;
			default:
				figActuelle.deSelectionne();
				figActuelle = new Quadrilatere(laCouleur,determinerRemplissage((String)choixRemplissage.getSelectedItem()));
			}
			figActuelle.selectionne();
			dfActuel.construit(figActuelle);
			dfActuel.repaint();
		}
		
		if(e.getSource()==this.exporter)
		{
			// Sert a exporter en PNG
			int nbExports = 0;
			BufferedReader source = null;
			PrintWriter destination = null;
			try {
				// Le fichier source sert a lire combien d'exportations ont deja ete effectuees
				source = new BufferedReader(new FileReader("nbExports.txt"));
				nbExports = Integer.parseInt(source.readLine());
				// On ecris le nouveau nombre d'exportations dans le meme fichier
				destination = new PrintWriter(new FileWriter("nbExports.txt"));
				destination.print(Integer.toString(nbExports+1));
				if(source!=null)
					source.close();
				if(destination!=null)
					destination.close();
			}
			catch(IOException e2) {
				System.out.println(e2.getMessage());
				System.out.println("Veuillez créer un fichier nbExports.txt et ecrire 0 dedans.");
			}
			
			// On sauvegarde ensuite le DessinFigures actuel
			BufferedImage BI = new BufferedImage(this.dfActuel.getWidth(), this.dfActuel.getHeight(), BufferedImage.TYPE_INT_RGB); 
			this.dfActuel.paint(BI.createGraphics());
			try {
				// Sous le nom exportX.png ou X est le numero de l'exportation
				ImageIO.write(BI,"png",new File("export"+(nbExports)+".png"));
			}
			catch (IOException e1) {
				System.out.println(e1.getMessage());
			}
		}
	}
	
	/**
	 * Methode permetant de determiner le remplissage actuelement selectionne dans le menu
	 * @param s -> chaine de caracteres seletionnee dans le menu
	 * @return boolean -> remplissage correspondant au boolean
	 */
	public boolean determinerRemplissage(String s)
	{
		switch(s)
		{
		case "Figure vide":
			return false;
		case "Figure remplie":
			return true;
		default :
			return true;
		}
	}
	
	/**
	 * Methode qui permet de changer la figure actuelement selectionnee
	 * @param f -> nouvelle figure selectionnee
	 */
	public void changerFigActuelle(FigureColoree f)
	{
		this.figActuelle = f;
	}
}
