import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Scribble implements MouseListener, MouseMotionListener{
  // Attributs
	/**
	 * Couleur actuelle des traces
	 */
	private Color couleur;
	/**
	 * Derniere ordonnee, derniere abscisse et dernier trace(nombre total de trace)
	 */
	private int lastX, lastY;
	
	
  // Constructeur
	public Scribble()
	{
	}
	
  // Methodes
	@Override
	public void mouseDragged(MouseEvent e)
	{
		((DessinFigures)e.getSource()).ajouterTrace(new Trace(this.lastX, this.lastY,e.getX(), e.getY(), this.couleur));
		this.lastX = e.getX();
		this.lastY = e.getY();
		((JComponent)(e.getSource())).repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)){
			this.lastX = e.getX();
			this.lastY = e.getY();
		}
	}
	
	/**
	 * Methode qui permet de modifier la couleur des traces
	 * @param c -> nouvelle Couleur
	 */
	public void setColor(Color c)
	{
		this.couleur = c;
	}
	


	@Override
	public void mouseMoved(MouseEvent arg0) {}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}
}
