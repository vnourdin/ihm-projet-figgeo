import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class DessinFigures extends JPanel{
  // Attributs
	/**
	 * liste qui contient les figures
	 */
	private ArrayList<FigureColoree> figures;
	/**
	 * indice de la figure actuellement selectionnee
	 */
	private int sel;
	/**
	 * Objet "listener" qui est utilise pour modifier les figures ou les deplacer
	 */
	private ManipulateurFormes mf;
	/**
	 * PanneauChoix associe, cet attribut sert simplement pour mettre a jour la figActuel du PanneauChoix
	 */
	private PanneauChoix pc;
	/**
	 * Scribble, dessin a main levee
	 */
	private Scribble sc;
	/**
	 * Liste des traces
	 */
	private ArrayList<Trace> listeTrace;
  
  // Constructeur
	/**
	 * Constructeur simple
	 */
	public DessinFigures()
	{
		this.figures = new ArrayList<FigureColoree>();
		this.sel = -1;
		this.mf = null;
		this.pc = null;
		this.sc = new Scribble();
		this.listeTrace = new ArrayList<Trace>();
	}
	
  // Methodes
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for (int i=0; i<this.figures.size(); i++)
		{
			figures.get(i).affiche(g);
		}
		
		if(this.listeTrace!=null)
		{
			for(int i=0; i<this.listeTrace.size(); i++){
				g.setColor(this.listeTrace.get(i).getCoul());
				g.drawLine(this.listeTrace.get(i).getDeb().x, this.listeTrace.get(i).getDeb().y, this.listeTrace.get(i).getFin().x, this.listeTrace.get(i).getFin().y);
			}
		}
	}
	
	public void ajoute(FigureColoree f)
	{
		if(f!=null)
		{
			this.figures.add(f);
			if(sel!=-1)
			{
				this.figures.get(sel).deSelectionne();
			}
			this.sel = this.figures.size()-1;
			this.figures.get(sel).selectionne();
		}
		this.repaint();
	}
	
	public void construit(FigureColoree fc)
	{
		this.supprimeAuditeurs();
		if(fc!=null)
		{
			this.addMouseListener(new FabricantFigures(fc));
		}
	}
	
	/**
	 * Permet de recuperer le tableau de figures
	 * @return FigureColoree[] -> tableau des figures de ce dessin
	 */
	public ArrayList<FigureColoree> getFigures()
	{
		return this.figures;
	}
	
	/**
	 * Permet de savoir quelle figure est actuellement selectionnee
	 * @return int -> indice de la figure selectionnee
	 */
	public int getSelected()
	{
		return this.sel;
	}
	
	/**
	 * Methode qui permet de changer de PanneauChoix associe
	 * @param pc -> nouveau panneauChoix associe
	 */
	public void setPanneauChoix(PanneauChoix pc)
	{
		this.pc = pc;
	}
	/**
	 * Methode qui permet de supprimer tous les auditeurs de la figure
	 */
	public void supprimeAuditeurs()
	{
		for(int i=0; i<this.getMouseListeners().length; i++)
			this.removeMouseListener(this.getMouseListeners()[i]);
		
		for(int i=0; i<this.getMouseMotionListeners().length; i++)
			this.removeMouseMotionListener(this.getMouseMotionListeners()[i]);
	}
	
	/**
	 * Methode qui permet d'activer le ManipulateurFormes du dessin et de passer en mode de modification
	 */
	public void activeManipulationsSouris()
	{
		this.mf = new ManipulateurFormes();
		this.addMouseListener(mf);
		this.addMouseMotionListener(mf);
	}
	
	/**
	 * Methode qui permet de vider le JPanel, d'effacer tout contenu
	 */
	public void vider()
	{
		this.figures.clear();
		this.sel = -1;
		
		this.listeTrace.clear();
	}
	
	public void supprimeFigSel()
	{
		if(sel!=-1)
			this.figures.remove(this.sel);
		sel = -1;
	}
	
	public void creerScribble()
	{
		this.sc = new Scribble();
		this.addMouseListener(this.sc);
		this.addMouseMotionListener(this.sc);
	}

	public void setScribbleColor(Color c)
	{
		this.sc.setColor(c);
	}
	
	public void ajouterTrace(Trace t)
	{
		this.listeTrace.add(t);
	}
	
	/**
	 * Classe interne de listerners qui sert a modifier et deplacer les figures
	 */
	public class ManipulateurFormes implements MouseListener, MouseMotionListener, java.util.EventListener {
		// Abscisse du clic
		private int last_x;
		// Ordonnée du clic
		private int last_y;
	  // Constructeur
		/**
		 * Constructeur simple
		 */
		 public ManipulateurFormes()
		 {
			 this.last_x = 0;
			 this.last_y = 0;
		 }
		 
	  // Methodes
		@Override
		public void mouseDragged(MouseEvent e){
			// Si l'utilisateur deplace la souris en restant appuye
			int deplacementX = e.getX() - last_x;
			int deplacementY = e.getY() - last_y;
			// Et qu'une figure est selectionnee
			if(sel!=-1 && figures.get(sel).estSelectionne())
			{
				// On la deplace pareillement a la souris
				figures.get(sel).translation(deplacementX, deplacementY);
				repaint();
			}
			// Puis on met a jour la derniere position de la souris
			last_x = e.getX();
			last_y = e.getY();
		}
		
		@Override
		public void mousePressed(MouseEvent e)
		{
			// Lorsque l'utilisateur appui sur la souris (on ne differencie pas les deux boutons)
			last_x = e.getX();
			last_y = e.getY();
			
			// On parcourt le tableau des figures pour voir si une figure est a cet endroit
			for(int i=(figures.size()-1); i>=0; i--)
			{
				// Si oui
				if(figures.get(i).estDedans(last_x, last_y))
				{
					// On deselectionne l'ancienne figure selectionnee
					if(sel!=-1)
						figures.get(sel).deSelectionne();
					sel = i;
					// On selectionne la figure actuelle
					figures.get(sel).selectionne();
					// On met a jour la figactuelle du PanneauChoix
					pc.changerFigActuelle(figures.get(sel));
					repaint();
					// On quitte la boucle
					break;
				}
				
				// Sinon
				else
				{
					// On deselectionne la figure actuellement selectionnee
					if(sel!=-1)
					{
						figures.get(sel).deSelectionne();
						sel = -1;
					}
					repaint();
				}
			}
		}
		
		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Si l'utilisateur effectue un double-clic sur une figure selectionnee : elle passe au premier plan
			if(e.getClickCount()==2)
			{
				if(sel!=-1 && figures.get(sel).estSelectionne())
				{
					// On copie la figure selectionnee dans une variable temporaire
					FigureColoree temp = figures.get(sel);
					// On supprime la figure selectionnee, la liste est donc triee automatiquement
					figures.remove(sel);
					// On ajoute la figure temporaire en fin de liste
					figures.add(temp);
				}
			}
		}
		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {}
		@Override
		public void mouseMoved(MouseEvent e){}
	}
}
